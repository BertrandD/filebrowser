var oDoc, sDefTxt, activeFile;

function initDoc() {
	console.log("call::initDoc()");
	oDoc = $("#textBox");
}

function formatDoc(sCmd, sValue) {
  document.execCommand(sCmd, false, sValue); 
  oDoc.focus();
}

function save(){
	console.log("call::save()");
	$.ajax({
		type:'post',
		url:'save.php',
		data:'text='+$('#textBox').html()+"&file="+activeFile,
		success:function(result){
			console.log(result);
		}
	});
}

function load(file){
	console.log("call::load("+file+")");
	$.ajax({
		type:'get',
		url:'load.php',
		data:'file='+file,
		success:function(result){
			oDoc.html(result);
		}
	});
}


function addTab(tabTitle, tabContent, tabId) {
  var label = tabTitle || "Tab " + tabCounter,
    id = "tab-"+tabId,
    li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) ),
    tabContentHtml = tabContent || "Tab " + tabCounter + " content.";

  tabs.find( ".ui-tabs-nav" ).append( li );
  oDoc.html(tabContentHtml);
  tabs.append( "<div id='" + id + "'></div>" );
  tabs.tabs( "refresh" );
  tabCounter++;
	tabs.tabs( "option", "active", -1 );
  tabsIndex[tabId]=tabs.tabs( "option", "active" ); 
}

function renameTab (tabId, tabName) {
	$("[aria-controls='tab-"+tabId+"'] a").html(tabName);
}

function removeTab(tabId){
	console.log("call::removeTab("+tabId+")");
	$( "[aria-controls='"+tabId+"']" ).remove();
    tabs.tabs( "refresh" );
}

function openTab(tabName,fileName){
	if($("#tab-"+fileName).length==0){
		$.ajax({
			type:'get',
			url:'load.php',
			data:'file='+fileName,
			success:function(result){
				addTab(tabName,result,fileName);
			}
		});
  	}else{
  		tabs.tabs( "option", "active", tabsIndex[fileName] );
  	}
}

$(document).ready(function(){
	tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>",
	tabCounter = 3;
    $.ajax({
    	type:'get',
    	url:'textEditor.html',
    	success:function(result){
    		$("#te").html(result);
			initDoc();
    	}
    });

    tabsIndex = Array();

	tabs = $( "#tabs" ).tabs({
	  	active: 1,
	  activate: function( event, ui ) {
	  	activeFile=$(ui.newPanel[0]).attr('id').replace("tab-","");
	  	console.log("Switching to "+activeFile);
	  	load(activeFile);
	  },
	  create:function(event,ui){
	  	activeFile=$(ui.panel[0]).attr('id')
	  	if(activeFile)
		  	load(activeFile);
	  }
	});

	tabs.delegate( "span.ui-icon-close", "click", function() {
	  var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
	  removeTab(panelId);
	});

	$("#tree").delegate("a","dblclick", function(e) {
	  	var fileName = $(this).parent().attr("id");
	  	var tabName = $(e.target).html().split("</i>")[1];
	  	openTab(tabName,fileName);
	});

	$(document).on('keydown', function(e){
	    if(e.ctrlKey && e.which === 83){ // Check for the Ctrl key being pressed, and if the key = [S] (83)
	    	save();
	        e.preventDefault();
	        return false;
	    }
	});

});