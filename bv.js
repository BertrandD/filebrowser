$( document ).ready(function() {
    $("#accordion" ).accordion({
    	heightStyle: "fill"
    });

		$('#tree').jstree({
                    'core' : {
                    		"animation" : 0,
                    		"check_callback" : true,
                    		"themes" : { "stripes" : true }, 
                            'data' : {"cache":false,"url" : "./root.json", "dataType" : "json" }// needed only if you do not supply JSON headers
                              },
                    "types" : {
                    			"#" : { "max_children" : 1, "max_depth" : 4, "valid_children" : ["root"] },
                   				 "root" : { 'icon':"./icons/folder.png", "valid_children" : ["default"] },
                    			"default" : { 'icon':"./icons/folder.png","valid_children" : ["default","file"] },
                    			"file" : { 'icon' :"./icons/blog.png", "valid_children" : [] }
                    			},
                    "plugins" : [ "contextmenu", "dnd", "state", "types", "wholerow"],
                    "contextmenu":{ "items": createmenu}
                  });


	// création du menu contextuel ouvert sur clic droit sur node
	function createmenu(node) 
	{
        var tree = $("#tree").jstree(true);
        return {
                    "item1": 
                    {
                        "label": "Create Directory",
                        "action":function () 
                        { 
                            node = tree.create_node(node);
                            tree.edit(node);
                        }
                    },

                    "item2": 
                    {
                        "label": "Create File",
                        "action": function () 
                        { 
                             node = tree.create_node(node,{"type":"file"});
                             tree.edit(node);
                        }
                    },
                          
                    "item3": 
                    {
                		"label": "Rename",
                		"action": function (obj) 
                		{ 
                			tree.edit(node);
                		}
            		},                         
            										
            		"item4": 
            		{
                		"label": "Remove",
                		"action": function (obj)
                		{ 
                			tree.delete_node(node);
                		}
            		}
        		};
    }

function save()
{   
            var v = $('#tree').jstree(true).get_json();
            var mytext = JSON.stringify(v);
            $.post("server_save.php",{data : mytext});
}  

$("#tree").on('create_node.jstree',function (e, data) {
                save();
               
                if(data.node.type == "file")
                     $.post("saveFile.php",{file : data.node.id});         

    openTab(data.node.text,data.node.id);
}) 

$("#tree").on('move_node.jstree',function (e, data) {
                save();
})  

$("#tree").on('delete_node.jstree',function (e, data) {
                save();
                deleteNodeOnServer(data.node);
    removeTab("tab-"+data.node.id);
                
})  

$("#tree").on('rename_node.jstree',function (e, data) {
                save();
    renameTab(data.node.id,data.node.text);            
})
});

function deleteNodeOnServer(data){

     if(data.type == "default")
     {
       for (var i=0;i< data.children.length;i++) 
       {
         node = $('#tree').jstree(true).get_node(data.children[i]);
         deleteNodeOnServer(node);
       }
     }
     else{
         $.post("removeFile.php",{file : data.id});
     }    
}

